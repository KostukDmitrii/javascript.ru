// Напишите деструктурирующее присваивание, которое:
// свойство name присвоит в переменную name.
// свойство years присвоит в переменную age.
// свойство isAdmin присвоит в переменную isAdmin (false, если нет такого свойства)
// Пример переменных после вашего присваивания:

let user = {
    name: "John",
    years: 30
  };

let {name, age:years, isAdmin = false} = user

// Создайте функцию topSalary(salaries), которая возвращает имя самого высокооплачиваемого сотрудника.
// Если объект salaries пустой, то нужно вернуть null.
// Если несколько высокооплачиваемых сотрудников, можно вернуть любого из них.

let salaries = {
    "John": 100,
    "Pete": 300,
    "Mary": 250
  };
  
  function topSalary(salaries) {
    if(Object.keys(salaries).length === 0) return null
    let valueArr = Object.values(salaries).sort((a,b)=>b-a)
    let biggestSalary = valueArr[0]
    for([worker, salary] of Object.entries( salaries)) {
      if(salary === biggestSalary) return worker
    }
  }

// ещё одно решение( предлагаемое JS.ru)


function topSalary(salaries) {
    max = 0
    maxName = null
    for(const [worker, salary] of Object.entries( salaries)) {
      if(max < salary) {
        max = salary
        maxName = worker
      } 
    }
    return maxName
  }

