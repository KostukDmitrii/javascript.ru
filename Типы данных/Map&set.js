// Допустим, у нас есть массив arr.

// Создайте функцию unique(arr), которая вернёт массив уникальных, не повторяющихся значений массива arr.


function unique(arr) {
    let result=[]
    let users = new Set()
    arr.forEach(user => users.add(user))
    for (let value of users) result.push(value)
    return result
}

let values = ["Hare", "Krishna", "Hare", "Krishna",
  "Krishna", "Krishna", "Hare", "Hare", ":-O"
];
// console.log(unique(values))

//ЭТАЛОН

function unique(arr) {
    return Array.from(new Set(arr));
  }

//   Напишите функцию aclean(arr), которая возвращает массив слов, очищенный от анаграмм.

//   let arr = ["nap", "teachers", "cheaters", "PAN", "ear", "era", "hectares"];
//   function aclean(arr) {
//     let splitedArr = arr.map(word => word.toLowerCase().split('').sort().join(''))
//     let searchedWords = new Map([
//         [splitedArr[0],  arr[0]],
//         [splitedArr[1],  arr[1]],
//         [splitedArr[2],  arr[2]],
//           ]);
//     return  searchedWords }
  

  function aclean(arr) {
    let map = new Map();
  
    for(let word of arr) {
      let sorted = word.toLowerCase().split("").sort().join("");
      map.set(sorted, word);
    }
  
    return Array.from(map.values());
  }
