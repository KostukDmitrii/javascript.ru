// Напишите функцию camelize(str), которая преобразует строки вида «my-short-string» в «myShortString».

// То есть дефисы удаляются, а все слова после них получают заглавную букву.

function camelize(str) {
    return str.split('-')
      .map((item, index) =>{
           if(index == 0){
             return item
      } else {
        return item[0].toUpperCase()+ item.slice(1)
      }})
      .join('')
  }

 camelize("background-color")

//  Напишите функцию filterRange(arr, a, b), которая принимает массив arr, ищет элементы со значениями больше или равными a и меньше или равными b и возвращает результат в виде массива.

// Функция должна возвращать новый массив и не изменять исходный.

let arr = [5, 3, 8, 1];

 function filterRange(arr, a, b) {
  return arr.filter(item => item >= a && item <=b)
 }

let filtered = filterRange(arr, 1, 4);

// alert (filtered ); // 3,1 (совпадающие значения)

// alert (arr ); // 5,3,8,1 (без изменений)

// Во-первых, реализуйте метод calculate(str), который принимает строку типа "1 + 2" в формате «ЧИСЛО оператор ЧИСЛО» (разделено пробелами) и возвращает результат. Метод должен понимать плюс + и минус -.
// Затем добавьте метод addMethod(name, func), который добавляет в калькулятор новые операции. Он принимает оператор name и функцию с двумя аргументами func(a,b), которая описывает его.

//Например, давайте добавим умножение *, деление / и возведение в степень **:

function Calculator() {

    this.methods = {
      "-": (a, b) => a - b,
      "+": (a, b) => a + b
    };
  
    this.calculate = function(str) {
  
      let split = str.split(' '),
        a = +split[0],
        op = split[1],
        b = +split[2]
  
      if (!this.methods[op] || isNaN(a) || isNaN(b)) {
        return NaN;
      }
  
      return this.methods[op](a, b);
    }
  
    this.addMethod = function(name, func) {
      this.methods[name] = func;
    };
  }

  // У вас есть массив объектов user, и в каждом из них есть user.name.
  //  Напишите код, который преобразует их в массив имён.

  let vasya = { name: "Вася", age: 25 };
let petya = { name: "Петя", age: 30 };
let masha = { name: "Маша", age: 28 };

let users = [ vasya, petya, masha ];

let names = users.map(item => item.name)

// alert( names ); // Вася, Петя, Маша

// У вас есть массив объектов user, и у каждого из объектов есть name, surname и id.
// Напишите код, который создаст ещё один массив объектов 
// с параметрами id и fullName, где fullName – состоит из name и surname.

let vasya1 = { name: "Вася", surname: "Пупкин", id: 1 };
let petya1 = { name: "Петя", surname: "Иванов", id: 2 };
let masha1 = { name: "Маша", surname: "Петрова", id: 3 };

let users1 = [ vasya1, petya1, masha1 ];

let usersMapped = users.map(
  item =>({
    fullName: `${item.name} ${item.surname}`,
    id:item.id
  })   
)

/*
usersMapped = [
   fullName: "Вася Пупкин",
  { fullName: "Петя Иванов", id: 2 },
  { fullName: "Маша Петрова", id: 3 }
]
*/

// alert(  usersMapped[0].fullName ) // Вася Пупкин
// alert( usersMapped[0].id ) // 1

// Напишите функцию sortByAge(users),
//  которая принимает массив объектов со свойством age и сортирует их по нему.

function sortByAge(arr) {
  arr.sort((a, b) => a.age > b.age ? 1 : -1);
}

let vasya2 = { name: "Вася", age: 25 };
let petya2 = { name: "Петя", age: 30 };
let masha2 = { name: "Маша", age: 28 };

let arr2 = [ vasya, petya, masha ];

sortByAge(arr);

 // теперь отсортировано: [vasya, masha, petya]
// alert(arr[0].name); // Вася
// alert(arr[1].name); // Маша
// alert(arr[2].name); // Петя

// Напишите функцию getAverageAge(users), которая принимает массив объектов со свойством 
// age и возвращает средний возраст.

// Формула вычисления среднего арифметического значения: (age1 + age2 + ... + ageN) / N.

let vasya3 = { name: "Вася", age: 25 };
let petya3 = { name: "Петя", age: 30 };
let masha3 = { name: "Маша", age: 29 };
let masha13 = { name: "Маша", age: 34 };


let arr3 = [ vasya3, petya3, masha3, masha13 ];

 function getAverageAge(arr) {
  let sumAge = arr.map(user => user.age)
  .reduce((sum, current) => current+sum)
  
return (sumAge / 3)
 }

//alert( getAverageAge(arr) ); // (25 + 30 + 29) / 3 = 28


function unique(arr) {
  let resultArr = []; 
  arr.map(str => {
     if ( !resultArr.includes(str)) {resultArr.push(str)}
  }
  )
  
  return resultArr
}

let strings = ["кришна", "кришна", "харе", "харе",
  "харе", "харе", "кришна", "кришна", ":-O"
];
// console.log(unique(strings))

let persons = [
  {id: 'john', name: "John Smith", age: 20},
  {id: 'ann', name: "Ann Smith", age: 24},
  {id: 'pete', name: "Pete Peterson", age: 31},
];

let usersById = groupById(persons);

  function groupById(arr) {
    return arr.reduce((obj, user) => {
      obj[user.id] = user
      return obj
  }, {});
    
  }