// Напишите функцию getWeekDay(date), 
// показывающую день недели в коротком формате: «ПН», «ВТ», «СР», «ЧТ», «ПТ», «СБ», «ВС».

let date = new Date(2012, 0, 3);  // 3 января 2012 года  

 function getWeekDay(date) {
   let week = ['ВС','ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ','СБ']
   
   return week[date.getDay()]
 }

//  В Европейских странах неделя начинается с понедельника (день номер 1), 
//  затем идёт вторник (номер 2) и так до воскресенья (номер 7). Напишите функцию getLocalDay(date), 
//  которая возвращает «европейский» день недели для даты date.

function getLocalDay(date) {

    let day = date.getDay();
  
    if (day == 0) { // день недели 0 (воскресенье) в европейской нумерации будет 7
      day = 7;
    }
  
    return day;
  }

//   Создайте функцию getDateAgo(date, days), возвращающую число, которое было days дней назад от даты date.
 
let date2 = new Date(2015, 0, 2);

function getDateAgo(date, days) {
  pastDate = new Date(date)
  pastDate.setDate(date.getDate() - days)
  return pastDate.getDate()
}

