// Создайте объект calculator (калькулятор) с тремя методами:

// read() (читать) запрашивает два значения и сохраняет их как свойства объекта.
// sum() (суммировать) возвращает сумму сохранённых значений.
// mul() (умножить) перемножает сохранённые значения и возвращает результат.

let calculator = {
    read() {
        this.a = prompt('a?',)
        this.b = prompt('b?',)
    },
    sum() {
        return Number(this.a) + Number(this.b)
    },
    mul(){
        return Number(this.a) * Number(this.b)
    }
  }

    calculator.read();
    alert( calculator.sum() );
    alert( calculator.mul() );

// У нас есть объект ladder (лестница), который позволяет подниматься и спускаться:

    // let ladder = {
    //     step: 0,
    //     up() {
    //       this.step++;
    //     },
    //     down() {
    //       this.step--;
    //     },
    //     showStep: function() { // показывает текущую ступеньку
    //       alert( this.step );
    //     }
    //   };

// Измените код методов up, down и showStep таким образом, чтобы их вызов можно было сделать по цепочке
     let ladder = {
        step: 0,
        up() {
          this.step++;
          return this
        },
        down() {
          this.step--;
          return this
        },
        showStep: function() { // показывает текущую ступеньку
          alert( this.step );
          return this
        }
      };
      ladder.up()