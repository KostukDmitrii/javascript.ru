// Напишите функцию sumTo(n), которая вычисляет сумму чисел 1 + 2 + ... + n.
// Сделайте три варианта решения:

// С использованием цикла.
// Через рекурсию, т.к. sumTo(n) = n + sumTo(n-1) for n > 1.
// С использованием формулы арифметической прогрессии.

function sumTo(n) {
    let x = 0
    let y = n
    for(let i= 0; i < n  ;i++) {
     x += y
     y--
    }
    return x
}

function sumToRec(n) {
    if( n == 1) {
        return n
    } else { 
        return n += sumToRec(n-1)
    }
}

function sumToProgression(n) {
    return n * (n + 1) / 2;
}



// Задача – написать функцию factorial(n), которая возвращает n!, используя рекурсию.

function factorial(n) {
    if(n == 1) return 1
     return n*factorial(n-1)
}

// Напишите функцию fib(n) которая возвращает n-е число Фибоначчи.

function fib(n){
    if(n == 1 ||n == 2) return 1
    return fib(n-1) + fib(n-2)
}

function fibFor(n) {
    let a = 1
    let b = 1
    for( let i = 3; i <= n; i++) {
        let c = a + b
        a = b
        b = c
    }
    return b
}


// Напишите функцию printList(list), которая выводит элементы списка по одному.

let list = {
    value: 1,
    next: {
      value: 2,
      next: {
        value: 3,
        next: {
          value: 4,
          next: null
        }
      }
    }
  };

  function printList(list) {
    let tmp = list;
  
    while (tmp) {
      console.log(tmp.value);
      tmp = tmp.next;
    }
  
  }
  
  function printListRec(list) {

    console.log(list.value); // выводим текущий элемент
  
    if (list.next) {
      printList(list.next); // делаем то же самое для остальной части списка
    }
  
  }
  
  printListRec(list);
