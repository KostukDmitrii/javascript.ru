//Напишите функцию sum, которая работает таким образом: sum(a)(b) = a+b.

function sum(a) {
    
   return function(b) {
        return a+b
    }
}

sum(2)(3)

// У нас есть встроенный метод arr.filter(f) для массивов. Он фильтрует все элементы с помощью функции f. Если она возвращает true, то элемент добавится в возвращаемый массив.

// Сделайте набор «готовых к употреблению» фильтров:

// inBetween(a, b) – между a и b (включительно).
// inArray([...]) – находится в данном массиве.
// Они должны использоваться таким образом:

// arr.filter(inBetween(3,6)) – выбирает только значения между 3 и 6 (включительно).
// arr.filter(inArray([1,2,3])) – выбирает только элементы, совпадающие с одним из элементов массива

let arr = [1, 2, 3, 4, 5, 6, 7];
  
  function inBetween(a, b) {
   return function(item) {
    //if(item >=a && item <=b) return item /моё решение Тесты непроходит, но результат получаю
    return item >=a && item <=b 

     }
  }

  function inArray(arr) {
    return function(item) {
        // if(arr.includes(item)) return item /моё решение Тесты непроходит, но результат получаю
        return arr.includes(item)

    }

  }

  arr.filter(inBetween(3, 6))

  arr.filter(inArray([11, 2, 7])) ; // 1,2

 // У нас есть массив объектов, который нужно отсортировать:

 let users = [
    { name: "John", age: 20, surname: "Johnson" },
    { name: "Pete", age: 18, surname: "Peterson" },
    { name: "Ann", age: 19, surname: "Hathaway" }
  ];

//Можем ли мы сделать его короче вместо функции мы просто писали byField(fieldName).

function byField(field) {
    return (a, b) => a[field] > b[field]? 1 : -1
}

users.sort(byField('name'))

// console.log(users.sort((a, b)=> a.name > b.name? 1 : -1))
//users.sort(byField('age'));
//////////////////////////////////////////////////////////////////////////////////////////


// function makeArmy() {
//     let shooters = [];
  
//     let i = 0;
    
//     while (i < 10) {
//         let shooter = function() { 
//             shooters.push(i)
//             return i
//          }
//          shooter()
//        i++
//     }
  
//     return shooters;
//   }
  
//   let army = makeArmy();
  
//   console.log(army[5])

  function makeArmy() {
    let shooters = [];
  
    let i = 0;
    while (i < 10) {
      let j = i
      let shooter = function() { // shooter function
      
        alert( j ); // should show its number
      };
      shooters.push(shooter);
      i++;
    }
  
    return shooters;
  }
  