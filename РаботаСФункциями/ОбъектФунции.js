function makeCounter() {  
     
    function counter(){
       return makeCounter.count++
       
    }
        counter.set = num => makeCounter.count = num
        counter.decrease = () => makeCounter.count--
        makeCounter.count = 0
    return counter
    }
     
  let counter = makeCounter();
  
  counter() ; // 0
   counter() ; // 1

  
  
  counter.set(10); // установить новое значение счётчика
  
   counter() ; // 10
  
  counter.decrease(); // уменьшить значение счётчика на 1
  
  counter() ; // 10 (вместо 11)

  /////////////////////////////////


  function sum (a){
    let currentSum = a

    function f(b){
         currentSum += b
         return f
    }

    f.toString = function(){
        return currentSum
    }

    return f
  }

 String(sum(1)(2)(3)(4))
  // в примере на js.ru используется alert который приводит выражение к строке для показа.
  // консоль такого не делает поэтому я использовал String() чтобы всё заработало



  function pigIt(str){
    return str.split(' ') // разбиваем на массив
      .map( item => { // собираем новый массив из нужных значений 
        if(item.toLowerCase() === item.toUpperCase){
          console.log( item)
          } else {
        return item = item.slice(1, item.length) + item.slice(0,1) +'ay'
            }
      })
      .join(' ') // собираем итоговое значение в строку, оно и будет возращено
  }

  console.log('!'.toLowerCase === item.toUpperCase)
  console.log(pigIt('Hello world !'))